﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;

public class UITime : MonoBehaviour
{
    public Text time;
    
    void Update()
    {
        time.text = GetTimeString(TimeManager.Instance.GetRemainingTime() + 1);
    }

    private string GetTimeString(float timeRemaining) {
        int minute = Mathf.FloorToInt(timeRemaining / 60);
        int second = Mathf.FloorToInt(timeRemaining % 60);

        return string.Format("{0} : {1}", minute.ToString(), second.ToString());
    }

    // Update is called once per frame
    public void Show() {
        gameObject.SetActive(true);
    } 
    public void Hide() {
        gameObject.SetActive(false);
    }
}
