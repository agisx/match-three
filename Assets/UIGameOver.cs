﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIGameOver : MonoBehaviour
{ 
    public void Show() {
        gameObject.SetActive(true);
    }
    public void Hide() {
        gameObject.SetActive(false);
    }

    private void Update() { 
        if (Input.GetMouseButtonUp(0)) {
            GameFlowManager.Instance.ResetGame();
            ScoreManager.Instance.ResetCurrentScore();
            TimeManager.Instance.ResetTheTimer();
            BoardManager.Instance.reCreateBoard();
        }
    }
}
