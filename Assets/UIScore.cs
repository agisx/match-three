﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UIScore : MonoBehaviour
{

    public Text highScore;
    public Text currentScore; 

    // Update is called once per frame
    void Update()
    {
        highScore.text = ScoreManager.Instance.HighScore.ToString();
        currentScore.text = ScoreManager.Instance.CurrentScore.ToString();
    }

    public void Show() {
        gameObject.SetActive(true);
    }public void HIde() {
        gameObject.SetActive(false);
    }
}
